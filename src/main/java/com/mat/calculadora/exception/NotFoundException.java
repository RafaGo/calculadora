package com.mat.calculadora.exception;

import org.springframework.http.HttpStatus;

public class NotFoundException extends Exception {

    public NotFoundException() {
        super(HttpStatus.NOT_FOUND.getReasonPhrase());
    }

}
