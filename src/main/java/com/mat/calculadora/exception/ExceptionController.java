package com.mat.calculadora.exception;

import java.util.Date;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ExceptionController {

    @ResponseStatus
    @ExceptionHandler(Exception.class)
    public ResponseEntity<CalculadoraException> genericError(Exception e) {
        var error = this.getError(HttpStatus.INTERNAL_SERVER_ERROR);
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(error);
    }

    @ResponseStatus
    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<CalculadoraException> errorException(Exception e) {
        var error = this.getError(HttpStatus.NOT_FOUND);
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(error);
    }

    private CalculadoraException getError(HttpStatus status) {
        return CalculadoraException.builder()
                .code(status.value())
                .message(status.getReasonPhrase())
                .timestamp(new Date().toString())
                .build();
    }
}