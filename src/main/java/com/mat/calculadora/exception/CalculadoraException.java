package com.mat.calculadora.exception;

import java.io.Serializable;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class CalculadoraException implements Serializable {

    private int code;
    private String message;
    private String timestamp;
}
