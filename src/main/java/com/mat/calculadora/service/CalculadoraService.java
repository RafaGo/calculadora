package com.mat.calculadora.service;

import org.springframework.stereotype.Service;

import com.mat.calculadora.dto.CalculadoraDto;
import com.mat.calculadora.dto.CalculadoraRespostaDto;
import com.mat.calculadora.dto.CalculadoraRespostaImpostoDto;
import com.mat.calculadora.exception.NotFoundException;

@Service
public class CalculadoraService {

    public CalculadoraRespostaDto calcular(CalculadoraDto calculadoraDto) throws NotFoundException {
        CalculadoraRespostaDto resposta = new CalculadoraRespostaDto();
        double resultado = 0;
        if (calculadoraDto.getOperacao().equals("+")) {
            resultado = calculadoraDto.getN1() + calculadoraDto.getN2();
        } else if (calculadoraDto.getOperacao().equals("*")) {
            resultado = calculadoraDto.getN1() * calculadoraDto.getN2();
        } else if (calculadoraDto.getOperacao().equals("-")) {
            resultado = calculadoraDto.getN1() - calculadoraDto.getN2();
        } else if (calculadoraDto.getOperacao().equals("/")) {
            resultado = calculadoraDto.getN1() / calculadoraDto.getN2();
        } else if (calculadoraDto.getOperacao().equals(null)) {
            throw new NotFoundException();
        }
        resposta.setResultado(resultado);
        return resposta;

    }

    public CalculadoraRespostaImpostoDto calculaImpostos(double salario) {
        CalculadoraRespostaImpostoDto resposta = new CalculadoraRespostaImpostoDto();
        double inss = salario * 0.08;
        double irpf = salario * 0.15;
        double salarioLiquido = (salario - inss) - irpf;
        resposta.setInss(inss);
        resposta.setIrpf(irpf);
        resposta.setSalario(salarioLiquido);
        return resposta;
    }
}
