package com.mat.calculadora.controller;

import java.net.URI;
import java.net.URISyntaxException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mat.calculadora.dto.CalculadoraDto;
import com.mat.calculadora.dto.CalculadoraRespostaDto;
import com.mat.calculadora.dto.CalculadoraRespostaImpostoDto;
import com.mat.calculadora.exception.NotFoundException;
import com.mat.calculadora.service.CalculadoraService;

@RequestMapping("/calcula")
@RestController
public class CalculadoraController {

    @Autowired
    CalculadoraService service;

    @PostMapping
    public ResponseEntity<CalculadoraRespostaDto> calculo(@RequestBody CalculadoraDto calculadoraDto)
            throws NotFoundException, URISyntaxException {
        CalculadoraRespostaDto resultado = service.calcular(calculadoraDto);
        return ResponseEntity.created(new URI("/calcula")).body(resultado);
    }

    @GetMapping("/impostos")
    public ResponseEntity<CalculadoraRespostaImpostoDto> calculaImpostos(@RequestParam Double salario)
            throws NotFoundException, URISyntaxException {
        CalculadoraRespostaImpostoDto resposta = this.service.calculaImpostos(salario);
        return ResponseEntity.ok(resposta);

    }
}
