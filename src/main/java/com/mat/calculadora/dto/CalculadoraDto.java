package com.mat.calculadora.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CalculadoraDto {
    private double n1;
    private double n2;
    private String operacao;

}
