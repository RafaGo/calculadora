package com.mat.calculadora.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CalculadoraRespostaImpostoDto {
    private double inss;
    private double irpf;
    private double salario;
}
