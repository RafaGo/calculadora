package com.mat.calculadora.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CalculadoraRespostaDto {
    private Double resultado;
}
