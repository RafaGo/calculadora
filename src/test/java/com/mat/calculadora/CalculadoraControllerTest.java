package com.mat.calculadora;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mat.calculadora.dto.CalculadoraDto;
import com.mat.calculadora.dto.CalculadoraRespostaImpostoDto;
import com.mat.calculadora.exception.NotFoundException;
import com.mat.calculadora.service.CalculadoraService;

@AutoConfigureMockMvc
@SpringBootTest
public class CalculadoraControllerTest {
    @Autowired
    private MockMvc mockMvc;
    private CalculadoraDto calcula;
    private CalculadoraRespostaImpostoDto imposto;
    private CalculadoraService service;

    @BeforeEach
    public void setUp() throws NotFoundException {
        this.calcula = new CalculadoraDto();
        calcula.setN1(2.0);
        calcula.setN2(1.0);
        calcula.setOperacao("+");
        calcula.setOperacao("*");
        calcula.setOperacao("/");
        calcula.setOperacao("-");
        calcula.setOperacao("==");
        this.service = new CalculadoraService();
        service.calculaImpostos(1000);
        service.calcular(calcula);
        this.imposto = new CalculadoraRespostaImpostoDto();
        imposto.setInss(1);
        imposto.setIrpf(2);
        imposto.setSalario(1000);

    }

    @Test
    public void CalculadoraTestPost() throws Exception {
        String strDto = new ObjectMapper().writeValueAsString(this.calcula);
        this.mockMvc.perform(post("/calcula").contentType("application/json").content(strDto))
                .andExpect(status().isCreated());
    }

}
